﻿using System.Web;
using System.Web.Mvc;

namespace _24HoursTravels
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}